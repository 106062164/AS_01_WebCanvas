# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
My canvas : https://106062164.gitlab.io/AS_01_WebCanvas/

There are 17 tools in my canvas, consists of 12 basic tools and 5 support tools. Basically, every time a tool button is clicked

Note : I do not post all the code in here, I just post the important part. The complete version can be seen in index.html and canvas.js.

These are 12 basic tools.
1.  pen.
    We can use the pen tool by press the 'pen' button. It is <button id="pen" type="button">Pen</button> in the HTML file.
    The pen is the default tool when you load the page. When you press your mouse on the canvas, it will scratch on the canvas. At any 'mousedown' event on the canvas, we will record the position of the cursor in x1, and y1.
    - (code)
      x1 = e.clientX - 300;
      y1 = e.clientY - 200;
    //-300 and -200 are because the position of the canvas.
    We can see that a sratch consists of many points, and those points can be circle, so I use line and circle to make the pen work.
    - (code)
      ctx.lineTo(e.clientX - 300, e.clientY - 200);
      ctx.stroke();
      ctx.beginPath();
      ctx.arc(e.clientX - 300, e.clientY - 200, size.value, 0, 2*Math.PI);
      ctx.fill();
      ctx.beginPath();
      ctx.moveTo(e.clientX - 300, e.clientY - 200);

2.  eraser.
    We can use the eraser tool by press the 'eraser' button. It is <button id="eraser" type="button">Eraser</button> in the HTML file.
    The eraser is used to erase anything on the canvas. If we think about eraser, actually it does the same thing with the thing pen does. Both of them scratch on the canvas, but the color of eraser is white, so it seems like it erases anything. The code is the same with the pen, except we add one more line to change the color.
    - (code)
      ctx.strokeStyle = "white";
      ctx.lineTo(e.clientX - 300, e.clientY - 200);
      ctx.stroke();
      ctx.beginPath();
      ctx.arc(e.clientX - 300, e.clientY - 200, size.value, 0, 2*Math.PI);
      ctx.fill();
      ctx.beginPath();
      ctx.moveTo(e.clientX - 300, e.clientY - 200);

After every act on the canvas, we store the changes in array history. I use function push() to do this job.
    function push() {
        var canvas = document.getElementById("myCanvas");
        var ctx = canvas.getContext("2d");
        if(sum < history.length) history.length = sum;
        history[sum] = canvas.toDataURL();
        sum++;
        ctx.beginPath();
    }
The code :
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    var oldCanvas = new Image();
    if(up == true) oldCanvas.src = history[sum];
    else oldCanvas.src = history[sum - 1];
    ctx.drawImage(oldCanvas, 0, 0);
is used in line, triangle, rectangle, and circle. We always clar and draw the image on canvas because we may move the mouse to drag or resize the text or shape but we only want the final result to be recorded.

3.  line.
    We can use the line tool by press the 'line' button. It is <button id="line" type="button">Line</button> in the HTML file.
    The line is used to create a straight line.
    - (code)
      ctx.beginPath();
      ctx.moveTo(x1, y1);
      ctx.lineTo(e.clientX - 300, e.clientY - 200);
      ctx.stroke();
      ctx.closePath();

4.  triangle.
    We can use the triangle tool by press the 'triangle' button. It is <button id="triangle" type="button">Triangle</button> in the HTML file.
    The triangle is used to create a triangle.
    - (code)
      ctx.beginPath();
      ctx.moveTo(x1, y1);
      ctx.lineTo(e.clientX - 300, e.clientY - 200);
      var dx = e.clientX - 300 - x1;
      ctx.lineTo(x1 - dx, e.clientY - 200);
      ctx.closePath();
      if(filled.checked) ctx.fill();
      ctx.stroke();
    //The closePath is used to connect the third point with (x1, y1). For the filled.checked, it will be explained in support tools.

5.  rectangle.
    We can use the rectangle tool by press the 'rectangle' button. It is <button id="rectangle" type="button">Rectangle</button> in the HTML file.
    The rectangle is used to create a rectangle.
    - (code)
      if(filled.checked) ctx.fillRect(x1, y1, e.clientX - 300 - x1, e.clientY - 200 - y1);
      else ctx.strokeRect(x1, y1, e.clientX - 300 - x1, e.clientY - 200 - y1)

6.  circle.
    We can use the circle tool by press the 'circle' button. It is <button id="circle" type="button">Circle</button> in the HTML file.
    The circle is used to create a circle.
    - (code)
      ctx.beginPath();
      var dx = (e.clientX > x1)? e.clientX - 300 - x1 : x1 - e.clientX - 300;
      var dy = (e.clientY > y1)? e.clientY - 200 - y1 : y1 - e.clientY - 200;
      var d = Math.sqrt((dx*dx)+(dy*dy));
      ctx.arc(x1, y1, d, 0, 2*Math.PI);
      if(filled.checked) ctx.fill();
      ctx.stroke();
      ctx.closePath();
    //The var d is the radius of the circle, with the center (x1, y1).

7.  undo.
    We can use the undo tool by press the 'undo' button. It is <button id="undo" type="button">Undo</button> in the HTML file.
    The undo is used to undo our act.
    - (code)
      if(sum > -1) {
          pushUn();
          sum--;
          ctx.clearRect(0, 0, canvas.width, canvas.height);
          var oldCanvas = new Image();
          oldCanvas.src = history[sum];
          oldCanvas.onload = function() {
              ctx.drawImage(oldCanvas, 0, 0);
          }
      }

8.  redo.
    We can use the redo tool by press the 'redo' button. It is <button id="redo" type="button">Redo</button> in the HTML file.
    The redo is used to redo our act. When undo is used, I store the undo history in array herstory. I use function pushUn() to do this job.
    - (code)
      if(cur > 0) {
          cur--;
          sum++;
          ctx.clearRect(0, 0, canvas.width, canvas.height);
          var oldCanvas = new Image();
          oldCanvas.src = herstory[cur];
          oldCanvas.onload = function() {
              ctx.drawImage(oldCanvas, 0, 0);
          }
      }

9.  reset.
    We can use the reset tool by press the 'reset' button. It is <button id="reset" type="button">Reset</button> in the HTML file.
    The reset is used to reset the canvas. We also need to clear and reset the history array and sum.
    - (code)
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      history = [];
      sum = 0;

10. text.
    We can use the text tool by press the 'text' button. It is <button id="text" type="button">Text</button> in the HTML file.
    The text is used to write text on the canvas. To operate this tool, first press the 'text' button, then type the text in the text field, which is located at the bottom. The text field is <p style="font-size:150%; color:#ffffff">Text<input type="text" id="sentence"></p> in the HTML file. After we type the text, go to the canvas and place the text by press the mouse. Before you really place the text, you may drag it until you find a best spot to place the text.
    - (code)
      ctx.drawImage(oldCanvas, 0, 0);
      ctx.fillText(sentence.value, e.clientX - 300, e.clientY - 200);

11. download.
    We can use the download tool by press the 'download' button. It is <button id="download" type="button">Download</button> in the HTML file.
    The download lets us download the objects on canvas as an image. I set the default name of the image to be "masterpiece.png".
    - (code)
      var image = canvas.toDataURL();
      var temp = document.createElement('a');
      temp.download = 'masterpiece.png'
      temp.href = image;
      document.body.appendChild(temp);
      temp.click();
      document.body.removeChild(temp);

12. upload.
    We can use the upload tool by press the 'browse' button. It is <input id="upload" type="file"> in the HTML file.
    The upload lets us upload a image and put it on the canvas.
    - (code)
      var image = new Image();
      image.src = URL.createObjectURL(document.getElementById('upload').files[0]);
      image.onload = function() {
          ctx.drawImage(image, 0, 0);
      };

These are 5 support tools, they are used to change the properties of basic tools.
1.  filled shape.
    We can use the filled shape tool by check the 'filled' button. It is <p style="font-size:150%; color:#ffffff">Filled<input type="checkbox" id="filled"></p> in the HTML file.
    This tool is used for triangle, rectangle, and circle. If we check the 'filled' button, the area inside triangle, rectangle, and circle will be filled by color too and vice versa.
    - triangle (code)
      if(filled.checked) ctx.fill();
    - rectangle (code)
      if(filled.checked) ctx.fillRect(x1, y1, e.clientX - 300 - x1, e.clientY - 200 - y1);
      else ctx.strokeRect(x1, y1, e.clientX - 300 - x1, e.clientY - 200 - y1)
    - circle (code)
      if(filled.checked) ctx.fill();

2.  color.
    We can use the color tool by press the 'color' input. It is <p style="font-size:150%; color:#ffffff">Color <input type="color" id="color"></p> in the HTML file.
    This tool is used to determine the color of the object we are going to draw. If we choose color blue, then everything (line, text, triangle, rectangle, circle) will have color blue. At any 'mousedown' event on canvas, the color will be updated.
    - (code)
      ctx.strokeStyle = color.value;
      ctx.fillStyle = color.value;
    //strokeStyle is for line and fillStyle is for area.

3.  line size.
    We can use the line size tool by scroll the 'line size' . It is <p style="font-size:150%; color:#ffffff"> Line Size <input type="range" id="size" min="1" max="40"></p> in the HTML file.
    We can increase or decrease the line size by scrolling the cursor. The smallest size is 1 and the biggest size is 40. Same with color, at any 'mousedown' event on canvas, the line size will be updated.
    - (code)
      ctx.lineWidth = 2*size.value;

4.  font type.
    We can use the font type tool by select the 'font type'. It is <select id="fontType">.....</select> in the HTML file.
    This tool is used to determine the font type of the text we are using. I provide 7 types of the font. Those 7 are Arial, Verdana, Times New Roman, Courier New, High Tower Text, serif, and sans-serif. Same with color, at any 'mousedown' event on canvas, the font type will be updated.
    - (code)
      ctx.font = fontSize.value + "px " + fontType.value;

5.  font size.
    We can use the font size tool by select the 'font size'. It is <select id="fontSize">.....</select> in the HTML file.
    This tool is used to determine the font size of the text we are using. I provide 16 kinds of the size, which is the same with the basic font size in Microsoft Word. Those 16 are 8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, and 72. Same with color, at any 'mousedown' event on canvas, the font size will be updated.
    - (code)
      ctx.font = fontSize.value + "px " + fontType.value;
    //As we can see, to update the text properties, we need both font type and font size.

The cursor icon may change, it depends on which tool we are using at that time.
1. The default cursor icon for pen, eraser, line, undo, redo, reset, download, and upload.
2. The crosshair cursor icon for triangle, rectangle, and circle.
3. The grab and grabbing cursor icon for text only.