var history = [];
var sum = 0;
var herstory = [];
var cur = 0;

function init() {
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    var pen = document.getElementById("pen");
    var eraser = document.getElementById("eraser");
    var triangle = document.getElementById("triangle");
    var rectangle = document.getElementById("rectangle");
    var circle = document.getElementById("circle");
    var line = document.getElementById("line");
    var text = document.getElementById("text");
    var undo = document.getElementById("undo");
    var redo = document.getElementById("redo");
    var reset = document.getElementById("reset");
    var upload = document.getElementById("upload");
    var download = document.getElementById("download");
    var filled = document.getElementById("filled");
    var color = document.getElementById("color");
    var size = document.getElementById("size");
    var fontType = document.getElementById("fontType");
    var fontSize = document.getElementById("fontSize");
    var sentence = document.getElementById("sentence");
    var cmd = "pen";
    var x1, y1;
    var draw = false;
    var un = false;
    var re = false;
    var up = false;
    
    pen.addEventListener('click', function() { 
        cmd = "pen";
        canvas.style.cursor = "default";
    });
    eraser.addEventListener('click', function() {
        cmd = "eraser";
        canvas.style.cursor = "default";
    });
    triangle.addEventListener('click', function() {
        cmd = "triangle";
        canvas.style.cursor = "crosshair";
    });
    rectangle.addEventListener('click', function() {
        cmd = "rectangle";
        canvas.style.cursor = "crosshair";
    });
    circle.addEventListener('click', function() {
        cmd = "circle";
        canvas.style.cursor = "crosshair";
    });
    line.addEventListener('click', function() {
        cmd = "line";
        canvas.style.cursor = "default";
    });
    text.addEventListener('click', function() {
        cmd = "text";
        canvas.style.cursor = "grab";
    });
    undo.addEventListener('click', function() {
        un = true;
        canvas.style.cursor = "default";
        if(sum > -1) {
            if(re == true) {
                re = false;
                herstory = [];
                cur = 0;
            }
            pushUn();
            sum--;
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            var oldCanvas = new Image();
            oldCanvas.src = history[sum];
            oldCanvas.onload = function() {
                ctx.drawImage(oldCanvas, 0, 0);
            }
        }
    });
    redo.addEventListener('click', function() {
        re = true;
        canvas.style.cursor = "default";
        if(cur > 0) {
            cur--;
            sum++;
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            var oldCanvas = new Image();
            oldCanvas.src = herstory[cur];
            oldCanvas.onload = function() {
                ctx.drawImage(oldCanvas, 0, 0);
            }
        }
    });
    reset.addEventListener('click', function() {
        canvas.style.cursor = "default";
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        history = [];
        sum = 0;
    });
    upload.addEventListener('change', function() {
        up = true;
        var image = new Image();
        image.src = URL.createObjectURL(document.getElementById('upload').files[0]);
        image.onload = function() {
            ctx.drawImage(image, 0, 0);
            push();
        };
    });
    download.addEventListener('click', function() {
        var image = canvas.toDataURL();
        var temp = document.createElement('a');
        temp.download = 'masterpiece.png'
        temp.href = image;
        document.body.appendChild(temp);
        temp.click();
        document.body.removeChild(temp);
    });

    canvas.addEventListener('mousedown', function(e){
        draw = true;
        x1 = e.clientX - 300;
        y1 = e.clientY - 200;
        ctx.strokeStyle = color.value;
        ctx.fillStyle = color.value;
        ctx.lineWidth = 2*size.value;
        ctx.font = fontSize.value + "px " + fontType.value;
        if(un == true) {
            un = false;
            sum++;
        }
        if(re == true) {
            re = false;
            herstory = [];
            cur = 0;
        }
        if(cmd == "pen") {
            ctx.lineTo(e.clientX - 300, e.clientY - 200);
            ctx.stroke();
            ctx.beginPath();
            ctx.arc(e.clientX - 300, e.clientY - 200, size.value, 0, 2*Math.PI);
            ctx.fill();
            ctx.beginPath();
            ctx.moveTo(e.clientX - 300, e.clientY - 200);
        } else if(cmd == "eraser") {
            ctx.strokeStyle = "white";
            ctx.fillStyle = "white";
            ctx.lineTo(e.clientX - 300, e.clientY - 200);
            ctx.stroke();
            ctx.beginPath();
            ctx.arc(e.clientX - 300, e.clientY - 200, size.value, 0, 2*Math.PI);
            ctx.fill();
            ctx.beginPath();
            ctx.moveTo(e.clientX - 300, e.clientY - 200);
        } else if(cmd == "text") {
            canvas.style.cursor = "grab";
            ctx.fillText(sentence.value, e.clientX - 300, e.clientY - 200);
        }
    });

    canvas.addEventListener('mousemove', function(e){
        if(draw) {
            if(cmd == "pen") {
                ctx.lineTo(e.clientX - 300, e.clientY - 200);
                ctx.stroke();
                ctx.beginPath();
                ctx.arc(e.clientX - 300, e.clientY - 200, size.value, 0, 2*Math.PI);
                ctx.fill();
                ctx.beginPath();
                ctx.moveTo(e.clientX - 300, e.clientY - 200);
            } else if(cmd == "eraser") {
                ctx.strokeStyle = "white";
                ctx.fillStyle = "white";
                ctx.lineTo(e.clientX - 300, e.clientY - 200);
                ctx.stroke();
                ctx.beginPath();
                ctx.arc(e.clientX - 300, e.clientY - 200, size.value, 0, 2*Math.PI);
                ctx.fill();
                ctx.beginPath();
                ctx.moveTo(e.clientX - 300, e.clientY - 200);
            } else if(cmd == "triangle") {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                var oldCanvas = new Image();
                if(up == true) oldCanvas.src = history[sum];
                else oldCanvas.src = history[sum - 1];
                ctx.drawImage(oldCanvas, 0, 0);
                ctx.beginPath();
                ctx.moveTo(x1, y1);
                ctx.lineTo(e.clientX - 300, e.clientY - 200);
                var dx = e.clientX - 300 - x1;
                ctx.lineTo(x1 - dx, e.clientY - 200);
                ctx.closePath();
                if(filled.checked) ctx.fill();
                ctx.stroke();
            } else if(cmd == "rectangle") {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                var oldCanvas = new Image();
                if(up == true) oldCanvas.src = history[sum];
                else oldCanvas.src = history[sum - 1];
                ctx.drawImage(oldCanvas, 0, 0);
                if(filled.checked) ctx.fillRect(x1, y1, e.clientX - 300 - x1, e.clientY - 200 - y1);
                else ctx.strokeRect(x1, y1, e.clientX - 300 - x1, e.clientY - 200 - y1)
            } else if(cmd == "circle") {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                var oldCanvas = new Image();
                if(up == true) oldCanvas.src = history[sum];
                else oldCanvas.src = history[sum - 1];
                ctx.drawImage(oldCanvas, 0, 0);
                ctx.beginPath();
                var dx = (e.clientX > x1)? e.clientX - 300 - x1 : x1 - e.clientX - 300;
                var dy = (e.clientY > y1)? e.clientY - 200 - y1 : y1 - e.clientY - 200;
                var d = Math.sqrt((dx*dx)+(dy*dy));
                ctx.arc(x1, y1, d, 0, 2*Math.PI);
                if(filled.checked) ctx.fill();
                ctx.stroke();
                ctx.closePath();
            } else if(cmd == "line") {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                var oldCanvas = new Image();
                if(up == true) oldCanvas.src = history[sum];
                else oldCanvas.src = history[sum - 1];
                ctx.drawImage(oldCanvas, 0, 0);
                ctx.beginPath();
                ctx.moveTo(x1, y1);
                ctx.lineTo(e.clientX - 300, e.clientY - 200);
                ctx.stroke();
                ctx.closePath();
            } else if(cmd == "text") {
                canvas.style.cursor = "grabbing";
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                var oldCanvas = new Image();
                oldCanvas.src = history[sum - 1];
                ctx.drawImage(oldCanvas, 0, 0);
                ctx.fillText(sentence.value, e.clientX - 300, e.clientY - 200);
            }
        }
    });

    canvas.addEventListener('mouseup', function(){
        up = false;
        draw = false;
        push();
    });
}

function push() {
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    if(sum < history.length) history.length = sum;
    history[sum] = canvas.toDataURL();
    sum++;
    ctx.beginPath();
}

function pushUn() {
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    if(cur < herstory.length) herstory.length = cur;
    herstory[cur] = canvas.toDataURL();
    cur++;
    ctx.beginPath();
}
        
window.onload = function () {
    init();
}